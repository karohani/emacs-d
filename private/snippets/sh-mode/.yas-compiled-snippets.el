;;; Compiled snippets and support files for `sh-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'sh-mode
                     '(("cget" "curl -X GET ${1:localhost}:${2:port}${3} -H ${4:'Content-Type: application/json'} ${5: -d \\'${6}\\'}  " "CURL_GET" nil nil nil "/Users/kakaopay/.emacs.d/private/snippets/sh-mode/curl_get" nil nil)))


;;; Do not edit! File generated at Fri Jun 15 17:17:06 2018
