;;; Compiled snippets and support files for `python-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'python-mode
                     '(("except" "try:\n    ${1:}\nexcept ${2:Exception} as e:\n    ${3:print(e)}\n\n" "except" nil nil nil "/Users/kakaopay/.emacs.d/private/snippets/python-mode/except" nil nil)))


;;; Do not edit! File generated at Fri Jun 15 17:17:06 2018
